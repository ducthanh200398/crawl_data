import atexit

from flask import Flask, current_app, jsonify
import pymongo
from apscheduler.schedulers.background import BackgroundScheduler

from services import articles, user, data_updater

app = Flask(__name__, instance_relative_config=True)
app.config.from_pyfile('config.py')

crontab = BackgroundScheduler(daemon=True)
crontab.add_job(data_updater.update, 'interval', minutes=60)
crontab.start()
atexit.register(lambda: crontab.shutdown(wait=False))

with app.app_context():
    client = pymongo.MongoClient(current_app.config["MONGO_CLIENT_NAME"])
    current_app.news_db = client['vnexpress']['educationNews']
    current_app.acc_db = client['vnexpress']['account']
    app.secret_key = current_app.config["SECRET_KEY"]

app.register_blueprint(articles.articles, url_prefix='/articles')
app.register_blueprint(user.user, url_prefix='/user')


@app.errorhandler(417)
def handle_input_err(err):
    return jsonify({'message': 'Input không hợp lệ', 'status_code': 417})


@app.errorhandler(401)
def handle_input_err(err):
    return jsonify({'message': 'Bạn phải đăng nhập để truy cập chức năng này', 'status_code': 401})


@app.errorhandler(404)
def handle_input_err(err):
    return jsonify({'message': 'Không tìm thấy đường dẫn', 'status_code': 404})


@app.errorhandler(422)
def handle_input_err(err):
    return jsonify({'message': 'Đường dẫn không hợp lệ', 'status_code': 422})


@app.errorhandler(405)
def handle_input_err(err):
    return jsonify({'message': 'Đường dẫn bị thiếu', 'status_code': 405})
