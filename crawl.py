import time
import multiprocessing as mp
import pymongo
import requests
from bs4 import BeautifulSoup
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


def get_session():
    session = requests.Session()
    retry = Retry(total=10,
                  read=10,
                  connect=10,
                  backoff_factor=1
                  )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def insert_data(data_queue):
    """Thêm dữ liệu từ data_queue vào mongodb.

    :param data_queue: chứa dữ liệu chờ được đẩy vào db.
    """

    my_client = pymongo.MongoClient(r"mongodb://localhost:27017/")
    vnexpress_database = my_client['vnexpress']
    education_news = vnexpress_database['educationNews']
    education_news.drop_indexes()
    stat_id = 1
    stop = False

    while not stop or not data_queue.empty():
        data = data_queue.get()
        if data == 'stop':
            stop = True
        else:
            print(stat_id)
            education_news.insert_one(data)
        stat_id = stat_id + 1

    education_news.create_index([('title', 1)])
    print('stop insert data')


def get_links(next_url, link_queue):
    """lấy đường dẫn các bài báo, đẩy vào link_queue.

    :param next_url: đường dẫn page đầu tiên.
    :param link_queue: chứa đường dẫn các bài báo chờ được trích xuất.
    """

    last_page = False
    session = get_session()
    while not last_page:
        print(next_url)
        page = session.get(next_url, allow_redirects=False)

        if page.status_code == 200:
            soup = BeautifulSoup(page.text, 'html.parser')
            for item in soup.select('.meta-news>.count_cmt'):
                link_queue.put(item['href'])
            next_url = 'https://vnexpress.net/' + soup.select_one('.next-page')['href']
        else:
            link_queue.put('stop')
            last_page = True

    print('stop get link')


def get_data(url, data_queue):
    """lấy ra nội dung bài báo theo đường dẫn, đẩy vào data_queue.

    :param url: đường dẫn bài báo cần trích xuất.
    :param data_queue: chứa dữ liệu chờ được đẩy vào db.
    """

    if url == 'stop':
        data_queue.put('stop')
        return 0

    session = get_session()
    page = session.get(url).text
    soup = BeautifulSoup(page, 'html.parser')

    try:
        title = soup.select_one('.title_news_detail,.title-detail,title').get_text()
    except (TypeError, AttributeError):
        title = ''

    content = soup.select('.fck_detail>p')
    try:
        content_text = str.join('\n', [p.get_text() for p in content])
    except (TypeError, AttributeError):
        print(f'fail {url}')
        content_text = []

    article_id = soup.select_one('meta[name="tt_article_id"]')['content']
    cmt_link = f'https://usi-saas.vnexpress.net/index/get?offset=0&limit=100000&frommobile=0&objectid={article_id}&objecttype=1&siteid=1000000&categoryid=1003497&cookie_aid=zlrwe9bw4ofhx58r'

    json_cmts = session.get(cmt_link).json()
    if json_cmts['data']:
        data_cmts = json_cmts['data']['items']
        cmts = [{'full_name': cmt['full_name'], 'content': cmt['content']} for cmt in data_cmts]
    else:
        cmts = []

    data = {
        'title': title,
        'url': url,
        'content': content_text,
        'comments': cmts
    }
    data_queue.put(data)


def crawl():
    """tao các luồng xử lý và hàng chờ:

    :param data_queue: chứa dữ liệu chờ đẩy vào database.
    :param link_queue chứa đường dẫn bài báo chờ được trích xuất dữ liệu.
    """

    url = 'https://vnexpress.net/giao-duc'
    manager = mp.Manager()
    data_queue = manager.Queue()
    link_queue = manager.Queue()
    pool = mp.Pool(22)
    pool.apply_async(insert_data, (data_queue,))
    pool.apply_async(get_links, (url, link_queue))
    stop = False

    while not stop:
        link = link_queue.get()
        if link == 'stop':
            stop = True
        pool.apply_async(get_data, (link, data_queue))
    pool.close()
    pool.join()


if __name__ == '__main__':
    stat = time.time()
    crawl()
    end = time.time()
    run_time = end - stat
    print(f'run time: {run_time}')
