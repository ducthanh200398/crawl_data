import pymongo
import requests
from bs4 import BeautifulSoup
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


def get_session():
    session = requests.Session()
    retry = Retry(total=10,
                  read=10,
                  connect=10,
                  backoff_factor=1
                  )
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    return session


def get_data(link, collection):
    """Lấy dữ liệu từ đường dẫn đổ vào db

    :param link: đường dẫn bài báo
    :param collection: db collection chứa dữ liệu bài báo
    """

    session = get_session()
    page = session.get(link).text
    soup = BeautifulSoup(page, 'html.parser')

    try:
        title = soup.select_one('.title_news_detail,.title-detail,title').get_text()
    except (TypeError, AttributeError):
        title = ''

    content = soup.select('.fck_detail>p')
    try:
        content_text = str.join('\n', [p.get_text() for p in content])
    except (TypeError, AttributeError):
        print(f'fail {link}')
        content_text = []

    article_id = soup.select_one('meta[name="tt_article_id"]')['content']
    cmt_link = f'https://usi-saas.vnexpress.net/index/get?offset=0&limit=100000&frommobile=0&objectid={article_id}&objecttype=1&siteid=1000000&categoryid=1003497&cookie_aid=zlrwe9bw4ofhx58r'

    json_cmts = session.get(cmt_link).json()
    if json_cmts['data']:
        data_cmts = json_cmts['data']['items']
        cmts = [{'full_name': cmt['full_name'], 'content': cmt['content']} for cmt in data_cmts]
    else:
        cmts = []

    data = {
        'title': title,
        'url': link,
        'content': content_text,
        'comments': cmts
    }
    collection.insert_one(data)
    print(link)


def update():
    """ Đến trang đàu tiên lấy toàn bộ đường dẫn,
    cập nhật thông tin các bài báo có đường dẫn không tồn tại trong db.
    """

    url = 'https://vnexpress.net/giao-duc'
    my_client = pymongo.MongoClient(r"mongodb://localhost:27017/")
    vnexpress_database = my_client['vnexpress']
    education_news = vnexpress_database['educationNews']
    education_news.drop_indexes()

    session = get_session()
    stop = False

    while not stop:
        response = session.get(url)
        soup = BeautifulSoup(response.text, 'html.parser')
        links = [item['href'] for item in soup.select('.meta-news>.count_cmt')]

        for link in links:
            n = education_news.count_documents({'url': link})
            print(n)
            if n == 0:
                get_data(link, education_news)
            else:
                stop = True

        url = 'https://vnexpress.net/' + soup.select_one('.next-page')['href']

    education_news.create_index([('title', 1)])


if __name__ == "__main__":
    update()
