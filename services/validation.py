import re

from flask import session
from werkzeug.exceptions import Unauthorized, ExpectationFailed
from functools import wraps


def check_access(func):
    """Kiểm tra xem người dùng có đang đăng nhập không"""

    @wraps(func)
    def decorated_func(*args, **kwargs):
        if 'username' not in session:
            raise Unauthorized
        return func(*args, **kwargs)

    return decorated_func


def check_text_input(input):
    """Kiểm tra xem input có hợp lệ hay không

    :app_config TEXT_REGEX_WHITELIST: chứa các kí tự chữ cái hoa thường
    có dấu và không có dấu, chữ số và 1 số kí tự không gây sql injection.

    :param input: đoạn string cần được kiểm tra
    """

    text_regex_whitelist = '[a-zA-Z0-9_ .@àáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹýÀÁÃẠẢĂẮẰẲẴẶÂẤẦẨẪẬÈÉẸẺẼÊỀẾỂỄỆĐÌÍĨỈỊÒÓÕỌỎÔỐỒỔỖỘƠỚỜỞỠỢÙÚŨỤỦƯỨỪỬỮỰỲỴỶỸÝ]'
    valid_input = re.findall(text_regex_whitelist, input)
    if len(input) != len(valid_input) or len(input) == 0:
        raise ExpectationFailed


def check_account_input(input):
    """Giống check_text_input nhưng không cho phép gõ dấu

    :app_config ACCOUNT_REGEX_WHITELIST: giống TEXT_REGEX_WHITELIST
    nhưng không chứa kí tự có dấu.

    :param input: đoạn string cần được kiểm tra
    """

    account_regex_whitelist = '[a-zA-Z0-9_.@]'
    valid_input = re.findall(account_regex_whitelist, input)
    if len(input) != len(valid_input) or len(input) == 0:
        raise ExpectationFailed
