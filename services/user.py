import hashlib

from flask import Blueprint, jsonify, current_app, session
from webargs import fields
from webargs.flaskparser import use_args

from .validation import check_account_input

user = Blueprint('user', __name__)


@user.route('/', methods=['GET', 'POST'])
@use_args({"username": fields.Str(missing=''), "password": fields.Str(missing='')}, location='form')
def do_login(form):
    """"Thực hiện đăng nhập

    Kiểm tra xem username, nếu các form input không hợp lệ raise exception,
    nếu các form input không khớp trong database thông báo đăng nhập thất bại
    nếu khớp thì lưu người dùng vào sesion, thông báo đăng nhập thành công.

    :form username: tên tài khoản
    :form password: mật khẩu
    """
    username = form['username']
    password = hashlib.md5(str(form['password']).encode('utf-8')).hexdigest()

    check_account_input(username)

    output = current_app.acc_db.count_documents({'username': username, 'password': password})
    if output == 1:
        session['username'] = username
        return jsonify({'message': 'Đăng nhập thành công'})
    else:
        return jsonify({'message': 'Tên tài khoản hoặc mật khẩu không chính xác'})
