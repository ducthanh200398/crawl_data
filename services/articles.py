from bson import ObjectId

from flask import Blueprint, request, current_app, jsonify
from webargs import fields
from webargs.flaskparser import use_args

from .validation import check_text_input, check_access

articles = Blueprint('articles', __name__)


@articles.route('/', methods=['GET'])
@use_args({"offset": fields.Int(missing=0), "limit": fields.Int(missing=100)}, location='querystring')
@use_args({"title": fields.Str(missing=''), "content": fields.Str(missing='')}, location='form')
def get_articles(arguments, form):
    """Lấy bài viết hỗ trợ phân trang và tìm kiếm theo tiêu đề, nội dung

    Tìm kiếm bài viết theo tiêu đề, nội dung. có hỗ trợ phân trang,
     lọc kí tự đặc biệt gây sql injection.

    :url_param offset: số bài viết bị bỏ qua, định dạng int, mặc định bằng 0
    :url_param limit: số bài viết hiển thị, định dạng int, mặc định bằng 10
    :url_param title: tiêu đề bài viết, có thể không có
    :url_param content: nội dung bài viết, có thể không có
    """

    offset = arguments["offset"]
    limit = arguments["limit"]

    query = {}
    if form['title']:
        title = form['title']
        check_text_input(title)
        query.update({'title': {'$regex': title, "$options": "i"}})
    if form['content']:
        content = form['content']
        check_text_input(content)
        query.update({'content': {'$regex': content, "$options": "i"}})

    data = list(current_app.news_db.find(query, {'_id': 0}).limit(limit).skip(offset))
    return jsonify(data)


@articles.route('/', methods=['POST'])
@use_args({"title": fields.Str(missing=''), "content": fields.Str(missing='')}, location='form')
@check_access
def create_article(form):
    """Tạo bài viết mới

    Thêm bài viết mới nếu người dùng đã đăng nhập, có hỗ trợ lọc những kí tự không hợp lệ.

    :form title: tiêu đề bài viết
    :form content: nội dung bài viết
    """

    title = form['title']
    content = form['content']

    check_text_input(title)
    check_text_input(content)

    current_app.news_db.insert_one({'title': title, 'content': content, 'comments': [], 'url': ''})
    return jsonify({'message': 'thêm thành công'})


@articles.route('/', methods=['PUT'])
@use_args({"id": fields.Int(required=True)}, location='querystring')
@use_args({"title": fields.Str(missing=''), "content": fields.Str(missing='')}, location='form')
@check_access
def update_article(arguments, form):
    """Cập nhật bài viết

    Cập nhật bài viết có id tương ứng nếu người dùng đã đăng nhập,
    có hỗ trợ lọc những kí tự không hợp lệ.

    :form title: tiêu đề bài viết
    :form content: nội dung bài viết

    :param id: ObjectId trong database
    """

    title = form['title']
    content = form['content']

    check_text_input(title)
    check_text_input(content)

    current_app.news_db.update_one({'_id': ObjectId(arguments['id'])}, {'$set': {'title': title, 'content': content}})
    return jsonify({'message': 'sửa thành công'})


@articles.route('/', methods=['DELETE'])
@use_args({"id": fields.Str(required=True)}, location='querystring')
@check_access
def delete_article(arguments):
    """Xóa bài viết

    Xóa bài viết có ObjectId tương ứng

    :param id: ObjectId trong database
    """

    current_app.news_db.delete_one({'_id': ObjectId(arguments['id'])})
    return jsonify({'message': 'xóa thành công'})
