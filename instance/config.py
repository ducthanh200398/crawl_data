DEBUG = True  # Turns on debugging features in Flask
BCRYPT_LOG_ROUNDS = 12  # Configuration for the Flask-Bcrypt extension
JSON_AS_ASCII = False

SECRET_KEY = b'3d6f45a5fc12445dbac2f59c3b6c7cb1'
MONGO_CLIENT_NAME = r"mongodb://localhost:27017/"
URL = 'https://vnexpress.net/giao-duc'
